To use this, add the following command to a .ssh/authorized_keys file:

command="git clone -q https://gitlab.com/malako/dotfiles.git $HOME/.markblaas/dotfiles; /bin/bash --rcfile $HOME/.markblaas/dotfiles/.bashrc; rm -rf $HOME/.markblaas/" ssh-rsa ...

If you are not me, please change the name of the folder where you clone in :)

You can also use the modified ssh-copy-id that takes a -c option. The syntax will be: ssh-copy-id -f -c 'command="git clone -q https://gitlab.com/malako/dotfiles.git $HOME/.markblaas/dotfiles; /bin/bash --rcfile $HOME/.markblaas/dotfiles/.bashrc; rm -rf $HOME/.markblaas/"' user@machine
