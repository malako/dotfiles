" Shortcuts
inoremap jk <ESC>
inoremap kj <ESC>

" Show Linenumbers
set number

" Handle tabs
set tabstop=4
set shiftwidth=4
set expandtab

" Don't expand tabs for makefiles
autocmd FileType make setlocal noexpandtab

" Remove tabs before saving
autocmd FileType c,cppm,ruby autocmd BufWritePre <buffer> retab

" Remove trailing whitespace before saving
autocmd FileType c,cpp,ruby autocmd BufWritePre <buffer> %s/\s\+$//e
